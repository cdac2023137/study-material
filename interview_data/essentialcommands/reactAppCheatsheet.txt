

### commands

### create new react app
npx create-react-app@5.0.0 blogs

```bash
  > cd blogs
  > yarn add react-router react-router-dom axios moment moment-timezone
```

### commands

- install yarn

```bash
  > npm install -g yarn
```

- create a new project

```bash
  > npx create-react-app newapp	---> doesn't support
```

- run a react application

```bash
  > cd newapp
  > yarn start
```





### file hierarchy

- **node_modules**
  - contains the node packages required to run the react application
- **public**:
  - contains the static data
  - e.g. css, index.html, images
- **src**:
  - contains the application source code
  - index.js:
    - entry point to the application
    - application starts with index.js
  - App.js
    - contains the first/startup component
- **.gitignore**
  - contains the list of files/directories which are not needed to add to the git repository
- **package.json**
  - contains the dependencies
- **README.md**
  - contains the read me instructions for the application
- **yarn.lock**
  - contains the packages versions used in the application
  - will get automatically created



### react routing

- documentation

  - https://reactrouter.com/docs/en/v6

- installation

```bash
  > yarn add react-router react-router-dom
```

### blogs

- create and configure the application

```bash
  > npx create-react-app blogs
  > cd blogs
  > yarn add react-router react-router-dom axios moment moment-timezone
```
yarn add axios


- packages
  - react-router and react-router-dom
    - needed for adding page/component switching functionality
  - axios
    - used for calling the REST apis
  - moment and moment-timezone
    - used for time/timezone conversion

WEBSITES--
reactrouter
react toastify