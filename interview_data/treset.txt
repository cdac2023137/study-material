anything implementing set, doesnt allow dups


TREESET
a treeSet is a simple set with only difference that it stores elements in sorted manner
--uses tree data structure (self balancing tree)for storage
--objects are stored in sorted ascending order
a set doesn’t retain the insertion order
treeset implements NavigableSet extends SortedSet extends set
treeset is an implemenatation of self balancing tree
therefore sortedSet provides us way to navigate through the tree.
insertion order is not retained in a tree set
Internally, for every element, the values are compared and sorted in the ascending order. 
No dups No nulls


HASHSET:
--implements set interface
--backed by Hashtable(underlying data structure )
--hash function disperses the elements properly among the buckets
--insertion order is not maintained. objects are inserted based on their hashCode
--null allowed

--
